// prueba de strings
const text = 'Hola Mundo';
// prueba de arreglo
const fruits = ['manzana', 'melon', 'banana'];

// test para prueba de string
test('Debe contener un texto', () => {
    expect(text).toMatch(/Mundo/);
});

// test para prueba de arreglo
test('Tenemos una banana?', () => {
    expect(fruits).toContain('banana');
});

// test para probar un número mayor que otro
test('Mayor que', () => {
    expect(10).toBeGreaterThan(9);
});

// test para probar booleans
test('Verdadero', () => {
    expect(true).toBeTruthy();
});

// Función para traer un string en reverse
const  reverseString = (str, callback) => {
    callback(str.split("").reverse().join(""));
};

// test para probar los callbacks
test('Probar un callback', () => {
    reverseString('hola', (str) => {
        expect(str).toBe('aloh');
    });
});

// funcion de promesa
const reverseString2 = (str) => {
    return new Promise((resolve, reject) => {
        if (!str) {
            reject(Error('Este es el Error'))
        }
        resolve(str.split("").reverse().join(""))
    });
};

// test para probar una promesa
test('Probar una promesa', () => {
    reverseString2('Hola').then(string => {
        expect(string).toBe('aloH');
    })
});

// test para probar funciones async y await
test('Probar async/await', async() => {
    const string = await reverseString2('hola');
    expect(string).toBe('aloh');
});

// funciones que se ejecutan antes y después de cada prueba
// afterEach(() => {
//     console.log('Después de cada prueba');
// });

// afterAll(() => {
//     console.log('Después de todas las pruebas');
// });

// beforeEach(() => {
//     console.log('Antes de cada prueba');
// });

// beforeAll(() => {
//     console.log('Antes de todas las pruebas');
// });